import os

from dotenv import load_dotenv
from flask import Flask, render_template, make_response
from flask_sqlalchemy import SQLAlchemy

load_dotenv()


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DB_URI')

db = SQLAlchemy(app)


with app.app_context():
    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/users')
    def users():
        users = []
        with open('./query1.sql', 'r') as f:
            for line in f.readlines():
                query = line.strip()
                result = db.session.execute(query)
                for row in result:
                    if isinstance(row[0], str) and len(row[0]) == 0:
                        continue
                    users.append(dict(id=row[0], first_name=row[1], last_name=row[2], full_name=f'{row[1]} {row[2]}'))
            db.session.commit()
        return make_response(users, 200)

    @app.route('/todos')
    def todos():
        todos = []
        with open('./query2.sql', 'r') as f:
            for line in f.readlines():
                query = line.strip()
                result = db.session.execute(query)
                for row in result:
                    if isinstance(row[0], str) and len(row[0]) == 0:
                        continue
                    todos.append(dict(id=row[0], user_id=row[1], subject=row[2], status=row[3], created_at=row[4], updated_at=row[5]))
            db.session.commit()
        return make_response(todos, 200)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=os.getenv('FLASK_RUN_PORT'))
